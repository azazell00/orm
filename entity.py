# -*- coding: utf-8 -*-

import psycopg2


class DatabaseError(Exception):
    pass
class NotFoundError(Exception):
    pass

class Entity(object):
    db = None


    __delete_query    = 'DELETE FROM "{table}" WHERE {table}_id=%s'
    __insert_query    = 'INSERT INTO "{table}" ({columns}) VALUES ({placeholders}) RETURNING "{table}_id"'
    __list_query      = 'SELECT * FROM "{table}"'
    __select_query    = 'SELECT * FROM "{table}" WHERE {table}_id=%s'
    __update_query    = 'UPDATE "{table}" SET {columns} WHERE {table}_id=%s'

    __parent_query    = 'SELECT * FROM "{table}" WHERE {parent}_id=%s'
    __sibling_query   = 'SELECT * FROM "{sibling}" NATURAL JOIN "{join_table}" WHERE {table}_id=%s'
    __update_children = 'UPDATE "{table}" SET {parent}_id=%s WHERE {table}_id IN ({children})'

    def __init__(self, id=None):
        if self.__class__.db is None:
            raise DatabaseError()

        self.__cursor   = self.__class__.db.cursor(cursor_factory=psycopg2.extras.DictCursor)
        self.__fields   = {}
        self.__id       = id
        self.__loaded   = False
        self.__modified = False
        self.__table    = self.__class__.__name__.lower()

    def __getattr__(self, name):
        self.__load()
        if self.__table + '_' + name in self.__fields:
            return self.__fields[self.__table + '_' + name]
        if name + '_id' in self.__fields:
            return self._get_parent(name)
        if name in self._siblings:
            return self._get_siblings(name)
        raise AttributeError

    def __setattr__(self, name, value):
        if name in self._columns:
            self.__fields[self.__table + '_' + name] = value
            self.__modified = True
        elif name in self._parents:
            self._set_parent(name, value)
            self.__modified = True
        else:
            super(Entity, self).__setattr__(name, value)

    def _get_parent(self, name):
        import models
        self.__execute_query(self.__parent_query.format(table=name, parent=name), (self.__fields[name + '_id'],))
        values = self.__cursor.fetchall()[0]
        cls = getattr(models, name.capitalize())
        parent_instance = cls()
        parent_instance.__id = values[0]
        parent_instance.__fields = dict(values)
        parent_instance.__loaded = True
        return parent_instance

    def _get_siblings(self, name):
        import models

        if self.__table < name:
            join_table = self.__table + '__' + name
        else:
           join_table = name + '__' + self.__table
        join_table = 'post__tag'
        self.__execute_query(self.__sibling_query.format(sibling=self._siblings[name].lower(), join_table=join_table, table=self.__table), (self.__id,))
        list = self.__cursor.fetchall()
        cls = getattr(models, self._siblings[name])
        instances = []
        for data_tuple in list:
            instance = cls()
            instance.__loaded = True
            instance.__id = data_tuple[0]
            instance.__fields = dict(data_tuple)
            instances.append(instance)
        return instances

    def _set_parent(self, name, value):
        if type(value) == int:
            self.__fields[name+ '_id'] = value
        else:
            self.__fields[name+ '_id'] = value.id

    def __load(self):
        if self.__loaded or not self.__id:
            return
        self.__execute_query(self.__select_query.format(table=self.__table), (self.__id, ))
        values = self.__cursor.fetchall()
        self.__fields = dict(values[0])
        self.__loaded = True

    def __execute_query(self, query, args=()):
        try:
            self.__cursor.execute(query, args)
        except:
            self.__class__.db.rollback()
            raise DatabaseError
        self.db.commit()

    def __insert(self):
        columns = ', '.join(self.__fields)
        placeholders = []
        for column in self.__fields:
            placeholders.append("'{}'".format(self.__fields[column]))
        self.__execute_query(self.__insert_query.format(table=self.__table, columns=columns,
                                                                      placeholders=', '.join(placeholders)))
        self.__id = self.__cursor.fetchone()[0]

    def __update(self):
        if not self.__modified:
            return
        columns = []
        for column in self.__fields:
            columns.append("{} = '{}'".format(column, self.__fields[column]))
        self.__execute_query(self.__update_query.format(table=self.__table,
                                                                      columns= ', '.join(columns)), (self.__id,))

    def save(self):
        if self.__id:
            self.__update()
        else:
            self.__insert()
        self.__modified = False

    @classmethod
    def all(cls):
        instances = []
        cursor = cls.db.cursor(
            cursor_factory=psycopg2.extras.DictCursor
        )
        cursor.execute(cls.__list_query.format(table=cls.__name__.lower()))
        list = cursor.fetchall()
        for data_tuple in list:
            instance = cls()
            instance.__loaded = True
            instance.__id = data_tuple[0]
            instance.__fields = dict(data_tuple)
            instances.append(instance)
        return instances

    def delete(self):
        self.__execute_query(self.__delete_query.format(table=self.__table), (self.__id,))

    @property
    def id(self):
        return self.__id

    @property
    def created(self):
        return self.__fields[(self._table + '_' + 'created')]

    @property
    def updated(self):
        return self.__fields[(self._table + '_' + 'updated')]